from django.shortcuts import render

from django.views.generic import TemplateView

from reader.models import  Author, Book
from reader.actions import get_books,get_author,get_list

class HomeView(TemplateView):
	template_name ='reader/home.html'
	#template_name ='testapp/home2.html'
	#template_name ='testapp/home3.html'
	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)

		context['author_name'] = get_author()
			

		context['books'] = get_books()
		context['list'] = get_list()

		return context
