from django.apps import AppConfig


class ReaderConfig(AppConfig):
	name = 'reader'
	verbose_name = "Reader"


	def ready(self):
		#import signals handlers
		#from critic import receiver
		import reader.signals
		 
		import critic.receivers