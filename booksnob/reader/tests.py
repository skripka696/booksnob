from django.test import TestCase
from reader import models

from reader import actions

class ActionsTestCase(TestCase):
	def setUp(self):
		self.Ben = models.Author.objects.create(first_name="Ben",
				last_name="B",email="ben@ukr.net")
		self.book_ben = models.Book.objects.create(author=self.Ben, 
													name="Ben book")

		self.Bill = models.Author.objects.create(first_name="Bill",
				last_name="B",email="bill@ukr.net")


		self.book_bill1 = models.Book.objects.create(author=self.Bill, name="Bill book1")
		self.book_bill2 = models.Book.objects.create(author=self.Bill, name="Bill book2")


	def test_get_author(self):
		authors = actions.get_author()
		self.assertEqual(authors,[self.Ben,self.Bill])
		
	def test_get_books(self):
		books = actions.get_books()
		self.assertEqual(books,[self.book_ben,self.book_bill1,self.book_bill2])

	def test_get_list(self):
		get_list = actions.get_list()
		self.assertEqual(get_list,[('Ben', ['Ben book']),
						('Bill', ['Bill book1', 'Bill book2'])])

